﻿using Exercice2Images.Models;
using Microsoft.AspNetCore.Mvc;

namespace Exercice2Images.Controllers
{
    public class GateauxController : Controller
    {
        private readonly List<Gateau> _mesGateaux;
        public GateauxController()
        {
            _mesGateaux = new List<Gateau>()
            { new Gateau(0, "Volcan et dinosaures", "/images/Dinos.jpg", "Gâteau génoise vanille, ganache au chocolat blanc vanille (intérieur) et au chocolat au lait (extérieur), amandes effilées", "Farine, oeufs, sucre, crème 35%, chocolat blanc, chocolat au lait, café, amandes, etc. "),
            new Gateau(1, "Mouton mignon", "/images/Sheep.jpg", "Gâteau en couches aux couleurs de l'arc-en-ciel avec une dacquoise amandes, crème chantilly mascarpone", "Farine, oeufs, sucre, amandes, crème 35%, mascarpone, guimauve, etc."),
            new Gateau(2, "Ruche", "/images/Ruche.jpg", "Gâteau vanille, ganache au chocolat blanc et miel", "Farine, oeufs, sucre, crème 35%, chocolat blanc, colorant jaune, miel, etc."),
            new Gateau(3, "Chat", "/images/chat.jpg", "Gâteau damier, génoise chocolat et vanille, crème au beurre meringue italienne, recouvert de pâte à sucre", "Farine, oeufs, sucre, beurre, colorant, etc."),
            new Gateau(4, "Echiquier", "/images/Echiquier.jpg", "Gâteau vanille et chocolat, crème au beurre italienne au beurre d'arachides, recouvert de pâte à sucre", "Farine, oeufs, sucre, vanille, beurre, beurre d'arachides, etc."),
            new Gateau(5, "Funfetti", "/images/funfetti.jpg", "Gâteau pinata, génoise vanille, crème chantilly mascarpone", "Farine, oeufs, sucre, crème 35%, mascarpone, etc."),
            new Gateau(6, "Halloween", "/images/Halloween.jpg", "Tarte panna cotta au chocolat, biscuits oreo, araignées en chocolat, glaçage au sucre", "Farine, oeufs, beurre, sucre, crème 35%, chocolat, etc."),
            };
        }


        /// <summary>
        /// Méthode d'action qui permet d'afficher 
        /// une liste d'images passées en paramètres 
        /// à la vue.
        /// </summary>
        /// <returns>La vue Liste</returns>
        public ViewResult Liste()
        {

            ViewBag.Titre = "Mes gâteaux";

            return View(_mesGateaux);
        }

        /// <summary>
        /// Méthode d'action qui permet d'afficher les 
        /// ingrédiens (ou autre) d'un gâteau
        /// </summary>
        /// <param name="id">Identifiant du gâteau</param>
        /// <returns>La vue Details</returns>
        public ViewResult DetailsGateau(int id)
        {
            int idGateau = _mesGateaux.FindIndex(g => g.Id == id);

            string ingredientsGateau = _mesGateaux[idGateau].Ingredients;
            string urlGateau = _mesGateaux[idGateau].UrlImage;

            ViewBag.Titre = "Détails gâteau";
            // L'idéal serait de créer un ViewModel (classe)
            // qui sera le modèle de la vue Details.
            // On passera alors un objet de ce type (cette classe)
            // à la vue Details.
            // Nous verrons cela une autre fois... En attendant,
            // nous nous contenterons d'envoyer des données faiblement
            // typées à la vue Details.
            ViewData["ingredientsGateau"] = ingredientsGateau;
            ViewData["urlGateau"] = urlGateau;

            return View("Details");
        }
    }
}
