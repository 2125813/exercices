﻿using Microsoft.AspNetCore.Mvc;

namespace Exercice2Images.Controllers
{
    public class AccueilController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
